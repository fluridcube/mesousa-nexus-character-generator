from security import sanitize_formatted_input
from data_objects.NexusCharacter import *
import os

image_path = os.environ['cloudfront_location'] + '/sheetimages/'


def build_image_path(filename: str) -> str:
    if filename:
        file_path: str = image_path + filename
        return file_path
    return ''


class NexusSheet(object):

    def __init__(self, sheet: NexusCharacter):
        self.character_object = sheet
        self.name = sheet.name
        self.series = sheet.series.name
        self.background = sheet.bio
        self.pic_small = build_image_path(sheet.image1)
        self.pic_large = build_image_path(sheet.image2)
        self.stats = StatsBlock(sheet.stats[0], sheet)
        self.skills = sheet.skills[0]
        self.powers = self.make_power_list()
        self.sheet_item = SheetItem(sheet.starting_items)
        self.startingItems = sheet.items

    def item_info(self):
        return self.sheet_item.item_info()

    def make_power_list(self):
        final_list = []
        index = 1
        for power in self.character_object.character_powers:
            final_list.append(SpecialAbility(power, index))
            index += 1
        return final_list


class StatsBlock:

    def __init__(self, stats: NexusStats, character: NexusCharacter):
        self.power = stats.pow
        self.athletics = stats.ath
        self.brains = stats.bra
        self.confidence = stats.con
        self.charisma = stats.cha
        self.affluence = stats.aff
        self.body = character.body
        self.energy = character.energy
        self.resilience = character.resilience


class SheetItem:
    type = 'Base'

    def __init__(self, sheet_item):
        self.item = sheet_item

    def item_info(self):
        if isinstance(self.item, NexusPokemon):
            return {'Name': self.item.name, 'Type': 'Pokemon', 'ImagePath': build_image_path(self.item.image),
                    'Description': self.generate_pokemon_des()}
        elif isinstance(self.item, NexusMecha):
            return {'Name': self.item.name, 'Type': 'Mecha', 'ImagePath': build_image_path(self.item.image), 'Description': self.generate_mecha_des()}
        else:
            return {'Name': self.item.name, 'Type': self.item.type, 'ImagePath': build_image_path(self.item.image),
                    'Description': self.generate_des()}

    def generate_des(self):
        item: NexusItems = self.item
        final_desc = item.description + ' <br>Integrity: ' + str(item.integrity) + '<br> HS: ' + str(item.holdout) + '<br>Cost: ' + str(item.cost)
        if item.uses != '':
            final_desc + "<br> Uses: " + str(item.uses)
        return final_desc

    def generate_pokemon_des(self):
        pkmn: NexusPokemon = self.item
        final_desc = "Power:" + str(pkmn.pow) + " Athletics: " + str(pkmn.ath) + " Run: " + str(pkmn.run) + " Fly: " + str(pkmn.fly) \
                     + ' Jump: ' + str(pkmn.jump) + ' H2H: ' + str(pkmn.h2h) + ' Dodge: ' + str(pkmn.dodge) + ' Grade: ' + str(pkmn.grade)
        if (pkmn.stage):
            final_desc += '<br> Evolution: ' + pkmn.stage
        final_desc += '<br>' + pkmn.moves
        return final_desc

    def generate_mecha_des(self):
        mecha: NexusMecha = self.item
        final_desc = "Power:" + str(mecha.pow) + " Athletics: " + str(mecha.ath) + " Run: " + str(mecha.run) + " Fly: " + str(mecha.fly) \
                     + ' Jump: ' + str(mecha.jump) + ' H2H: ' + str(mecha.h2h) + ' Dodge: ' + str(mecha.dodge) + ' Grade: ' + str(mecha.grade) + \
                     '<br>' + mecha.abilities
        return final_desc


class SpecialAbility:

    def __init__(self, power: NexusPowers, index):
        self.powerName = power.name
        self.powerSkill = power.skill
        self.powerCost = power.cost
        self.powerDescription = power.desc
        self.position = index
