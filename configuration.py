import os

def auth_zero_domain():
    return os.environ['AUTH0_DOMAIN']


def auth_zero_encrypt():
    list = []
    list.extend(os.environ['ALGORITHMS'])
    return list


def client_id():
    return os.environ['client_id']


def client_secret():
    return os.environ['secrets']


def access_token():
    return os.environ['ACCESS_TOKEN']


def auth_url():
    return os.environ['AUTHORIZE_URL']


def callback_url():
    return os.environ['CALLBACK_URL']