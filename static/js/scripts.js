$(document).ready(function () {
	// displays the menu for all dropSearches
	$(".dropSearch-button").on('click', function (){
		$(this).parent().find(".dropSearch-content").toggle();
	});
	
	// updates dropSearch with what was selected
	$(".dropSearch .dropSearch-content span").on('click', function(){
		var selectedOption = $(this).attr('value');
		var selectedName = $(this).html();
		$(this).parent().parent().find(".dropSearch-button").html(selectedName);
		$(this).parent().parent().find(".dropSearch-content").toggle();
		$(this).parent().parent().find(".dropSearch-input").val(selectedOption);
		console.log("selectedOption = " + selectedOption + " -- selectedOption just for visible purposes for now, would be in function for that specific dropsearch")
	});
	
	// these two actions are for dropdowns - drop the menu + update initial text with selection
	$(".dropdown").on('click', function(){
		$(this).find("ul").toggle();
	});
	$(".dropdown span[value]").on('click', function(){
		var selectedName = $(this).html();
		var selectedOption = $(this).attr('value')
		$(this).parent().parent().parent().find(".dropdown-initial").html(selectedName);
		$(this).parent().parent().parent().find(".dropdown-form-input").val(selectedOption);
	});
	
	// when something is selected in the dropdown for a sheet item, it makes that content visible
	$("#dropdown-seriesChoice span").on('click', function(){
		var selectedOption = $(this).attr('value');
		if (selectedOption == "newSeries"){
			$(".new-series").show();
		}
		else{
			$(".new-series").hide();
			console.log("Do whatever needs to happen on the backend here.");
		}
		resizeAllGridItems();
  	});

	// when something is selected in the dropdown for a sheet item, it makes that content visible
	$("#dropdown-sheetItemChoice span").on('click', function(){
		var selectedOption = $(this).attr('value');
		$(".sheet-item-possibility").hide();
		$(".sheet-item-possibility.sheet-item-"+selectedOption).show();
		resizeAllGridItems();
  	});
	
	// when a pokemon is chosen, it links a version of that pokemon to the character
	$("#dropdown-pokemonChoice span").on('click', function(){
		var selectedOption = $(this).attr('value');
		if (selectedOption == "newPokemon"){
			$(".new-pokemon").show();
		}
		else{
			$(".new-pokemon").hide();
			console.log("Do whatever needs to happen on the backend here.");
		}
		resizeAllGridItems();
	});
	
	// on load or resize, it auto-heights all the sections of the grid
	window.onload = resizeAllGridItems();
	var resizeId;
	$(window).resize(function() {
		clearTimeout(resizeId);
		resizeId = setTimeout(doneResizing, 500);
	});

	$(".dropSearch-input").keyup(function() {filterFunction(this);});
});

// next several functions are for automatic grid heights and resize
function resizeGridItem(item){
	var grid = document.getElementsByClassName("page")[0];
	var rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'));
	var rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'));
	var rowSpan = Math.ceil(((item.querySelector('.content').getBoundingClientRect().height+rowGap)/(rowHeight+rowGap))+2);
    item.style.gridRowEnd = "span "+rowSpan;
}
function resizeAllGridItems(){
	var allItems = document.getElementsByClassName("creation-section");
	for(i = 0; i < allItems.length; i++){
		resizeGridItem(allItems[i]);
	}
}
function doneResizing(){
    resizeAllGridItems();
}

// filters series dropdown during search
function filterFunction(dropdown) {
	var dropSearchInput = $(dropdown);
	var filter = dropSearchInput.val().toUpperCase();
	var a = $(dropSearchInput).parent().find("span");
	for (i = 0; i < a.length; i++) {
		txtValue = a[i].textContent || a[i].innerText;
		if (txtValue.toUpperCase().indexOf(filter) > -1) {
			a[i].style.display = "";
		} else {
			a[i].style.display = "none";
		}
	}
}

// gets google profile of signed in user
/*function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}

// sign out of google
function signOut() {
	var auth2 = gapi.auth2.getAuthInstance();
	auth2.signOut().then(function () {
		console.log('User signed out.');
	});
}*/