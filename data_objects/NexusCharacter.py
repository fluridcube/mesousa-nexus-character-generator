from dataclasses import dataclass
from data_objects import ImageData
from typing import List


@dataclass
class NexusObject:
    pass


@dataclass
class NexusSeries:
    id: int
    name: str
    medium: int


@dataclass
class NexusItems:
    id: int
    type: str
    name: str
    description: str
    series: NexusSeries
    two_sided: bool
    back_desc: str
    is_sheet_item: bool
    integrity: int
    holdout: int
    uses: int
    image: ImageData
    cost: int


@dataclass
class NexusPowers:
    name: str
    skill: str
    cost: int
    desc: str
    based_on: int


@dataclass
class NexusTypes:
    id: int
    type: str


@dataclass
class NexusPowerTypes:
    power_id: int
    type_id: int


@dataclass
class NexusSkills:
    acting: int
    archery: int
    authority: int
    bFG: int
    blades: int
    blunt: int
    computerUse: int
    deception: int
    disguise: int
    dodge: int
    h2h: int
    hideStuff: int
    influence: int
    jump: int
    magicChi: int
    marksmanship: int
    mechaWeapons: int
    mechanics: int
    medicine: int
    pilotMecha: int
    pilotVehicle: int
    run: int
    science: int
    search: int
    specialWeapon: int
    stealth: int
    yeet: int
    yoink: int
    authorityOver: str


@dataclass
class NexusStats:
    pow: int
    ath: int
    bra: int
    con: int
    cha: int
    aff: int


@dataclass
class NexusCharacter:
    id: int
    name: str
    bio: str
    image1: str
    image2: str
    series: NexusSeries
    resilience: int
    body: int
    energy: int
    character_powers: List[NexusPowers]
    skills: List[NexusSkills]
    stats: List[NexusStats]
    starting_items: NexusItems
    items: str


@dataclass
class NexusPokemon:
    id: int
    name: str
    image: str
    type: str
    stage: str
    body: int
    energy: int
    resilience: int
    grade: int
    pow: int
    ath: int
    run: int
    fly: int
    jump: int
    h2h: int
    dodge: int
    moves: str


@dataclass
class NexusMecha:
    id: int
    name: str
    image: str
    integrity: int
    energy: int
    resilience: int
    grade: int
    pow: int
    ath: int
    run: int
    fly: int
    jump: int
    h2h: int
    dodge: int
    abilities: str
