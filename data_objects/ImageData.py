from dataclasses import dataclass

@dataclass
class ImageData:
    id: int
    file_location: str
    scale: float
    translate_x: int
    translate_y: int

