from boto3 import client
from os import environ
from botocore.exceptions import ClientError
import logging
from werkzeug.datastructures import FileStorage

class ImageStore:

    def __init__(self):
        self.s3 = client('s3')
        self.cloudfrontid = environ['cloudfront_location']
        self.bucketname = environ['s3_bucket']

    def get_image(self, filename: str) -> str:
        image_source: str = self.cloudfrontid + filename
        return image_source

    def post_image(self, data, filename: str):
        if isinstance(data, FileStorage):
            object_data = data
        elif isinstance(data, str):
            try:
                object_data = open(data, 'rb')
            except Exception as e:
                logging.error(e)
                return False
        else:
            logging.error('Type of ' + str(type(data)) +
                          ' for the argument \'src_data\' is not supported.')
            return False

        try:
            self.s3.put_object(Bucket=self.bucketname, Key='sheetimages/'+filename, Body=object_data)
        except ClientError as e:
            logging.error(e)
            return False
        finally:
            if isinstance(data, str):
                object_data.close()
        return True
