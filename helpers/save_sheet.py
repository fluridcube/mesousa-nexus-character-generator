import os
from helpers.image_store import ImageStore
from werkzeug.utils import secure_filename
from helpers.database import CharacterCreator
from time import time

class SheetSaver:

    def __init__(self, aws: ImageStore, db):
        self.aws: ImageStore = aws
        self.db = CharacterCreator()

    @staticmethod
    def allowed_filename(filename):
        return '.' in filename and \
               filename.rsplit('.', 1)[1].lower() in ['jpg', 'png', 'gif']

    def save_sheet(self, request) -> int:
        form_data = request.form
        image_locations = self.parse_images(request, form_data)
        character_id = self.db.create_character_and_save(form_data, image_locations)
        return character_id

    def parse_images(self, request, form_data):
        file_dict = {}
        if 'char-pic-small' in request.files:
            file_dict['char-pic-small'] = self.save_image(request.files['char-pic-small'], form_data['series-name'], form_data['char-name'],
                                                          'portrait')
        if 'char-pic-large' in request.files:
            file_dict['char-pic-large'] = self.save_image(request.files['char-pic-large'], form_data['series-name'], form_data['char-name'],
                                                          'large_pic')
        if form_data.get('char-item-type', 'item') == 'item' and 'char-item-item-picture' in request.files:
            file_dict['char-item-item-picture'] = self.save_image(request.files['char-item-item-picture'], form_data['series-name'],
                                                                  form_data['char-name'], 'sheet_item')
        if form_data.get('char-item-type', 'item') == 'mecha' and 'char-item-mecha-picture' in request.files:
            file_dict['char-item-mecha-picture'] = self.save_image(request.files['char-item-mecha-picture'], form_data['series-name'],
                                                                   form_data['char-name'], 'sheet_item')
        if form_data.get('char-item-type', 'item') == 'companion' and 'char-item-companion-picture' in request.files:
            file_dict['char-item-companion-picture'] = self.save_image(request.files['char-item-companion-picture'], form_data['series-name'],
                                                                       form_data['char-name'], 'sheet_item')
        if form_data.get('char-item-type', 'item') == 'pokemon' and 'pokemon-picture' in request.files:
            file_dict['char-item-pokemon-picture'] = self.save_image(request.files['pokemon-picture'], form_data['series-name'],
                                                                   form_data['char-name'], 'sheet_item')
        return file_dict

    def save_image(self, file, series_name, character_name, type):
        filename = str()
        if self.allowed_filename(file.filename):
            filename = secure_filename(
                series_name + '_' + character_name + '_' + type + str(int(time())) + os.path.splitext(file.filename)[1])
            self.aws.post_image(file, filename)
        return filename
