from data_objects.NexusCharacter import NexusCharacter, NexusItems, NexusSeries, NexusPowers, NexusSkills, NexusStats, NexusPokemon, NexusMecha
from helpers.image_store import ImageStore
from security import sanitize_formatted_input
from helpers.sheet_item_parsers import SheetItemMecha, SheetItemCompanion, SheetItemPokemon


class GetSeries:

    def __init__(self):
        pass

    def get_series_from_db(self, series_id):
        return NexusSeries(series_id, 'Test Series', 5)

    def create_new_series(self, series_name):
        if not self.series_exists(series_name):
            return NexusSeries(1, 'New Series', 5)
        else:
            return self.get_series_from_db(12345)

    def series_exists(self, series_name):
        return False


class GetPokemon:

    def __init__(self):
        pass

    def get_pokemon_from_db(self, pokemon_id):
        return NexusPokemon()

    def create_new_pokemon(self, pokemon_name):
        if not self.pokemon_exists(pokemon_name):
            return NexusPokemon()
        else:
            return self.get_pokemon_from_db(self.get_pokemon_from_name(pokemon_name))

    def pokemon_exists(self, pokemon_name):
        return False

    def get_pokemon_from_name(self, pokemon_name):
        return 12345


class CharacterCreator:

    def __init__(self):
        # self.db = self.Connect()
        pass

    # def Connect(self):
    #     pass

    def create_character_and_save(self, form_data, image_locations):
        character_series = self.generate_series(form_data)
        character_skills = self.generate_skills(form_data)
        character_stats = self.generate_stats(form_data)
        character_items = self.generate_items(form_data, image_locations)
        character_powers = self.generate_powers(form_data)

        character = NexusCharacter(None, form_data['char-name'], sanitize_formatted_input(form_data['char-background']),
                                   image_locations.get('char-pic-small', None), image_locations.get('char-pic-large', None), character_series,
                                   form_data['char-resilience'], form_data['char-body'], form_data['char-energy'], character_powers, character_skills,
                                   character_stats, character_items, sanitize_formatted_input(form_data['char-startingItems']))
        #        character.id = self.db.save(character)
        return character

    @staticmethod
    def generate_series(form_data):
        series_db = GetSeries()
        if form_data.get('series-id', None) and form_data['series-id'] != 'newSeries':
            series = series_db.get_series_from_db(form_data['series-id'])
        else:
            series = series_db.create_new_series(form_data['series-name'])
        return series

    @staticmethod
    def generate_skills(form_data):
        skills = NexusSkills(form_data['char-acting']
                             , form_data['char-archery']
                             , form_data['char-authority']
                             , form_data['char-bfg']
                             , form_data['char-blades']
                             , form_data['char-blunt']
                             , form_data['char-computerUse']
                             , form_data['char-deception']
                             , form_data['char-disguise']
                             , form_data['char-dodge']
                             , form_data['char-h2h']
                             , form_data['char-hideStuff']
                             , form_data['char-influence']
                             , form_data['char-jump']
                             , form_data['char-magicChi']
                             , form_data['char-marksmanship']
                             , form_data['char-mechaWeapons']
                             , form_data['char-mechanics']
                             , form_data['char-medicine']
                             , form_data['char-pilotMecha']
                             , form_data['char-pilotVehicle']
                             , form_data['char-Run']
                             , form_data['char-science']
                             , form_data['char-search']
                             , form_data['char-specialWeapons']
                             , form_data['char-stealth']
                             , form_data['char-throw']
                             , form_data['char-yoink']
                             , form_data['char-authorityOver'])
        return [skills]

    @staticmethod
    def generate_stats(form_data):
        stats = NexusStats(form_data['char-power'], form_data['char-athletics'], form_data['char-brains'], form_data['char-confidence'],
                           form_data['char-charisma'], form_data['char-affluence'])
        return [stats]

    def generate_items(self, form_data, image_locations):
        sheet_item = self.make_sheet_item(form_data, image_locations)
        return sheet_item

    def make_sheet_item(self, form_data, image_locations):
        if form_data.get('char-item-type', 'Item') == 'pokemon':
            return self.sheet_item_pokemon(form_data, image_locations)
        elif form_data.get('char-item-type', 'Item') == 'mecha':
            return self.sheet_item_mecha(form_data, image_locations)
        elif form_data.get('char-item-type', 'Item') == 'companion':
            return self.sheet_item_companion(form_data, image_locations)
        else:
            return self.sheet_item(form_data, image_locations)

    @staticmethod
    def sheet_item(form_data, image_locations):
        item = NexusItems(None,form_data['char-item-item-type'] ,form_data['char-item-item-name'], sanitize_formatted_input(form_data['char-item-item-description']), None, False,
                          None, True, form_data['char-item-item-integrity'], form_data['char-item-item-HS'], form_data['char-item-item-uses'],
                          image_locations.get('char-item-item-picture', None), form_data['char-item-item-cost'])
        return item

    @staticmethod
    def sheet_item_mecha(form_data, image_locations):
        item = NexusMecha(None, form_data['char-item-mecha-name'], image_locations.get('char-item-mecha-picture', None),
                          form_data['char-item-mecha-integrity'], form_data['char-item-mecha-energy'], form_data['char-item-mecha-resilience'],
                          form_data['char-item-mecha-grade'], form_data['char-item-mecha-power'], form_data['char-item-mecha-athletics'],
                          form_data['char-item-mecha-run'], form_data['char-item-mecha-fly'], form_data['char-item-mecha-jump'],
                          form_data['char-item-mecha-h2h'], form_data['char-item-mecha-dodge'],
                          sanitize_formatted_input(form_data['char-item-mecha-abilities']))
        return item

    @staticmethod
    def sheet_item_companion(form_data, image_locations):
        companion = SheetItemCompanion(form_data)
        item = NexusPokemon(None, companion.name, image_locations.get('char-item-companion-picture', None), 'Companion', None, companion.body,
                            companion.energy, companion.resilience, companion.grade, companion.power, companion.athletics, companion.run,
                            companion.fly, companion.jump, companion.h2h, companion.dodge, companion.abilities)
        return item

    @staticmethod
    def sheet_item_pokemon(form_data, image_locations):
        pkmn = SheetItemPokemon(form_data)
        item = NexusPokemon(None, pkmn.name, image_locations.get('char-item-pokemon-picture', None), 'Pokemon', pkmn.evolutionStage, pkmn.body,
                            pkmn.energy, pkmn.resilience, pkmn.grade, pkmn.power, pkmn.athletics, pkmn.run, pkmn.fly, pkmn.jump, pkmn.h2h, pkmn.dodge,
                            pkmn.moves)
        return item

    def generate_powers(self, form_data):
        final_list = []
        index = 1
        while True:
            if form_data.get('char-power' + str(index) + 'Name', None) is not None:
                final_list.append(self.special_ability(form_data, index))
                index += 1
            else:
                break
        return final_list

    @staticmethod
    def special_ability(form_data, index):
        ability = NexusPowers(form_data['char-power' + str(index) + 'Name'], form_data['char-power' + str(index) + 'Skill'],
                              form_data['char-power' + str(index) + 'Cost'],
                              sanitize_formatted_input(form_data['char-power' + str(index) + 'Description']), None)
        return ability
