from security import sanitize_formatted_input

class SheetItem:
    type = 'Base'

    def __init__(self, form_data):
        self.name = form_data['char-item-item-name']
        self.type = form_data['char-item-item-type']
        self.picture = form_data['char-item-item-picture']
        self.description = sanitize_formatted_input(form_data['char-item-item-description'])
        self.integrity = form_data['char-item-item-integrity']
        self.HS = form_data['char-item-item-HS']
        self.cost = form_data['char-item-item-cost']
        self.uses = form_data['char-item-item-uses']

    def item_info(self):
        return {'Name': self.name, 'Type': self.type, 'ImagePath': self.picture, 'Description': self.generate_des()}

    def generate_des(self):
        final_desc = self.description + ' <br>Integrity: ' + self.integrity + '<br> HS: ' + self.HS + '<br>Cost: ' + self.cost
        if self.uses != '':
            final_desc + "<br> Uses: " + self.uses
        return final_desc


class SheetItemMecha:
    type = 'Mech'

    def __init__(self, form_data):
        self.name = form_data['char-item-mecha-name']
        self.integrity = form_data['char-item-mecha-integrity']
        self.energy = form_data['char-item-mecha-energy']
        self.resilience = form_data['char-item-mecha-resilience']
        self.grade = form_data['char-item-mecha-grade']
        self.power = form_data['char-item-mecha-power']
        self.athletics = form_data['char-item-mecha-athletics']
        self.run = form_data['char-item-mecha-run']
        self.fly = form_data['char-item-mecha-fly']
        self.jump = form_data['char-item-mecha-jump']
        self.h2h = form_data['char-item-mecha-h2h']
        self.dodge = form_data['char-item-mecha-dodge']
        self.abilities = sanitize_formatted_input(form_data['char-item-mecha-abilities'])

    def item_info(self):
        return {'Name': self.name, 'Type': 'Mecha', 'ImagePath': self.picture, 'Description': self.generate_des()}

    def generate_des(self):
        final_desc = 'Pow:' + self.power + ' Ath:' + self.athletics + ' Run:' + self.run + '  Fly: ' + self.fly + \
                     '<br>Jump:' + self.jump + ' H2H:' + self.h2h + ' Dodge:' + self.dodge + ' Integrity:' + \
                     self.integrity + '<br>Grade:' + self.grade + ' Energy:' + self.energy + '<br>' + self.abilities
        return final_desc


class SheetItemCompanion:
    type = 'Companion'

    def __init__(self, form_data):
        self.name = form_data['char-item-companion-name']
        self.body = form_data['char-item-companion-body']
        self.energy = form_data['char-item-companion-energy']
        self.resilience = form_data['char-item-companion-resilience']
        self.grade = form_data['char-item-companion-grade']
        self.power = form_data['char-item-companion-power']
        self.athletics = form_data['char-item-companion-athletics']
        self.run = form_data['char-item-companion-run']
        self.fly = form_data['char-item-companion-fly']
        self.jump = form_data['char-item-companion-jump']
        self.h2h = form_data['char-item-companion-h2h']
        self.dodge = form_data['char-item-companion-dodge']
        self.abilities = sanitize_formatted_input(form_data['char-item-companion-abilities'])

    def item_info(self):
        return {'Name': self.name, 'Type': 'Companion', 'ImagePath': self.picture, 'Description': self.generate_des()}

    def generate_des(self):
        final_desc = 'Pow:' + self.power + ' Ath:' + self.athletics + ' Run:' + self.run + '  Fly: ' + self.fly + \
                     '<br>Jump:' + self.jump + ' H2H:' + self.h2h + ' Dodge:' + self.dodge + ' Body:' + \
                     self.body + '<br>' + self.grade() + ' Energy:' + self.energy + '<br>' + self.abilities
        return final_desc

    def grade(self):
        if self.grade != '':
            return 'Grade:' + self.grade
        else:
            return ''


class SheetItemPokemon:
    type = 'Pokemon'

    def __init__(self, form_data):
        self.name = form_data['pokemon-name']
        self.evolutionStage = form_data['pokemon-evolution']
        self.body = form_data['pokemon-body']
        self.energy = form_data['pokemon-energy']
        self.resilience = form_data['pokemon-resilience']
        self.grade = form_data['pokemon-grade']
        self.power = form_data['pokemon-power']
        self.athletics = form_data['pokemon-athletics']
        self.run = form_data['pokemon-run']
        self.fly = form_data['pokemon-fly']
        self.jump = form_data['pokemon-jump']
        self.h2h = form_data['pokemon-h2h']
        self.dodge = form_data['pokemon-dodge']
        self.moves = sanitize_formatted_input(form_data['pokemon-moves'])

    def item_info(self):
        return {'Name': self.name, 'Type': 'Pokemon', 'ImagePath': self.picture, 'Description': self.generate_des()}

    def generate_des(self):
        final_desc = 'Pow:' + self.power + ' Ath:' + self.athletics + ' Run:' + self.run + '  Fly: ' + self.fly + \
                     '<br>Jump:' + self.jump + ' H2H:' + self.h2h + ' Dodge:' + self.dodge + ' Body:' + \
                     self.body + '<br>' + self.grade() + ' Energy:' + self.energy + '<br>' + self.moves
        return final_desc

    def grade(self):
        if self.grade != '':
            return 'Grade:' + self.grade
        else:
            return ''