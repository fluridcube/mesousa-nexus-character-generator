from flask import Flask, render_template, url_for, session, redirect, request, abort
from flask_bootstrap import Bootstrap
from SheetGenerator.Sheet_Object import NexusSheet
from authorization import AuthError, requires_auth, auth_register, get_token_auth_header
from json import dumps
from six.moves.urllib.parse import urlencode
import configuration
import secrets
from security import render_csp_template
from helpers.image_store import ImageStore
from helpers.save_sheet import SheetSaver

mesousa = Flask(__name__)
mesousa.secret_key = configuration.client_secret()
auth0 = auth_register(mesousa)
image_store = ImageStore()
sheet_saver = SheetSaver(image_store, None)

@mesousa.before_request
def csrf_protect():
    if request.method == "POST":
        token = session.get('_csrf_token', None)
        if not token or token != request.form.get('_csrf_token'):
            abort(403)


def generate_csrf_token():
    if '_csrf_token' not in session:
        session['_csrf_token'] = secrets.token_urlsafe(256)
    return session['_csrf_token']


@mesousa.route('/')
@requires_auth
def hello():
    return render_csp_template(render_template('dashboard.html', userinfo=session.get('profile', None)))


@mesousa.route('/welcome')
def welcome():
    return render_csp_template(render_template('login.html', userinfo=session.get('profile',None)))


@mesousa.route('/login')
def login():
    return auth0.authorize_redirect(redirect_uri=configuration.callback_url())


@mesousa.route('/logout')
def logout():
    # Clear session stored data
    session.clear()
    # Redirect user to logout endpoint
    params = {'returnTo': url_for('welcome', _external=True), 'client_id': configuration.client_id()}
    return redirect(auth0.api_base_url + '/v2/logout?' + urlencode(params))


@mesousa.route('/callback')
def callback_handling():
    auth0.authorize_access_token()
    resp = auth0.get(configuration.auth_zero_domain() + '/userinfo')
    userinfo = resp.json()

    session['jwt_payload'] = userinfo
    session['profile'] = {
        'user_id': userinfo['sub'],
        'name': userinfo['name'],
        'picture': userinfo['picture']
    }

    return redirect('/dashboard')


@mesousa.route('/dashboard')
@requires_auth
def dashboard():
    return render_csp_template(render_template('dashboard.html',
                               userinfo=session['profile'],
                               userinfo_pretty=dumps(session['jwt_payload'], indent=4)))


@mesousa.route('/chracter_search')
@requires_auth
def char_search():
    return render_csp_template(render_template('base.html', userinfo=session['profile']))


@mesousa.route('/sheet-creation')
@requires_auth
def creator():
    return render_csp_template(render_template('sheet_creator.html', userinfo=session['profile']))


@mesousa.route('/approvals')
@requires_auth
def approver():
    return render_csp_template(render_template('base.html', userinfo=session['profile']))


@mesousa.route('/generate_single_sheet', methods=["Post"])
@requires_auth
def generate_single_sheet():
    sheet = sheet_saver.save_sheet(request)
    sheetlist = []
    sheetlist.append(NexusSheet(sheet))
    return render_csp_template(render_template("GenerateSheetList.html", selectedsheets=sheetlist,
                               stylesheet="page_one_warmonger.css",
                               template="NexusCompactStatPage.html"))


@mesousa.errorhandler(AuthError)
def handle_auth_error(ex):
    return "Look, I'm not sure why you're here. If you should be here, ask a GM to get you setup. If you shouldn't" \
           "well then, why are you here?"


@mesousa.errorhandler(403)
def four_oh_threed(e):
    return render_csp_template(render_template("errorpage.html", error ="Forbidden!"))


@mesousa.errorhandler(404)
def four_oh_foured(e):
    return render_csp_template(render_template("errorpage.html", error ="That Page Doesn't Exist"))


mesousa.jinja_env.globals['csrf_token'] = generate_csrf_token

if __name__ == '__main__':
    Bootstrap(mesousa)
    mesousa.run(debug=True)
