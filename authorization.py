import json
from flask import request, session, redirect
from functools import wraps
from jose import jwt
from urllib.request import urlopen
import configuration
from authlib.flask.client import OAuth

AUTH0_DOMAIN = configuration.auth_zero_domain()
ALGORITHMS = configuration.auth_zero_encrypt()


class AuthError(Exception):
    def __init__(self, error, status_code):
        self.error = error
        self.status_code = status_code


def get_token_auth_header():
    """Obtains the Access Token from the Authorization Header
    """
    auth = request.headers.get('Authorization', None)
    if not auth:
        raise AuthError({
            'code': 'authorization_header_missing',
            'description': 'Authorization header is expected.'
        }, 401)

    parts = auth.split()

    if parts[0].lower() != 'bearer':
        raise AuthError({
            'code': 'invalid_header',
            'description': 'Authorization header must start with "Bearer".'
        }, 401)

    elif len(parts) == 1:
        raise AuthError({
            'code': 'invalid_header',
            'description': 'Token not found.'
        }, 401)

    elif len(parts) > 2:
        raise AuthError({
            'code': 'invalid_header',
            'description': 'Authorization header must be bearer token.'
        }, 401)

    token = parts[1]
    return token


# /server.py

def requires_auth(f):
  @wraps(f)
  def decorated(*args, **kwargs):
    if 'profile' not in session:
      # Redirect to Login page here
      return redirect('/welcome')
    return f(*args, **kwargs)

  return decorated


def auth_register(webapp):
    oauth = OAuth(webapp)
    auth0 = oauth.register(
        'auth0',
        client_id=configuration.client_id(),
        client_secret=configuration.client_secret(),
        api_base_url=configuration.auth_zero_domain(),
        access_token_url=configuration.access_token(),
        authorize_url=configuration.auth_url(),
        client_kwargs={'scope':'openid profile'})
    return auth0
